CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table Test (
	id uuid DEFAULT uuid_generate_v4(),
	name varchar(50) not null
)

