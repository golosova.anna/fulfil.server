﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FulFil.Data.DbUp
{
    public static class Extensions
    {
        public static IWebHostBuilder UseDbUpgrade<T>(this IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceProvider = services.BuildServiceProvider();
                var config = serviceProvider.GetService<IConfiguration>();
                var logger = serviceProvider.GetService<ILogger<DatabaseFilter<T>>>();

                var dbLogger = new DbUpLogger<DatabaseFilter<T>>(logger);
                var connectionString = config.GetValue<string>("db:connectionString");

                services.AddSingleton<IStartupFilter>(new DatabaseFilter<T>(connectionString, dbLogger));
            });
            return builder;
        }
    }

}
