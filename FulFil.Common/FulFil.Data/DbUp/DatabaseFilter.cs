﻿using DbUp;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System;

namespace FulFil.Data.DbUp
{
    public class DatabaseFilter<T> : IStartupFilter
    {
        private readonly string _connectionString;
        private readonly DbUpLogger<DatabaseFilter<T>> _logger;

        public DatabaseFilter(string connectionString, DbUpLogger<DatabaseFilter<T>> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {
            EnsureDatabase.For.PostgresqlDatabase(_connectionString);

            var dbUpgradeEngineBuilder = DeployChanges.To
                .PostgresqlDatabase(_connectionString)
                .WithScriptsEmbeddedInAssembly(typeof(T).Assembly)
                .WithTransaction()
                .LogTo(_logger);

            var dbUpgradeEngine = dbUpgradeEngineBuilder.Build();
            if (dbUpgradeEngine.IsUpgradeRequired())
            {
                _logger.WriteInformation("Upgrades have been detected. Upgrading database now...");
                var opertation = dbUpgradeEngine.PerformUpgrade();
                if (opertation.Successful)
                {
                    _logger.WriteInformation("Upgrade completed successfully");
                }
                else
                {
                    _logger.WriteInformation("Error happened in the upgrade. Please check the logs");
                }
            }

            return next;
        }
    }

}
