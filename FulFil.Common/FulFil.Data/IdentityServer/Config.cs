﻿using IdentityModel;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace FulFil.Data.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId()
            };

        public static IEnumerable<ApiResource> Apis =>
            new List<ApiResource>
            {
                new ApiResource(Constants.FUL_FIL_API_NAME, "Main API of FulFilCounter application")
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = Constants.MOBILE_APP_CLIENT_ID,

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret(Constants.CLIENT_SECRET.ToSha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { Constants.FUL_FIL_API_NAME }
                }
            };
    }
}
