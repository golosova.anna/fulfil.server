﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FulFilCounter.API.Controllers
{
    [ApiController]
    [Route("/api/wishes")]
    public class WishesController : ControllerBase
    {
        private readonly ILogger<WishesController> _logger;

        public WishesController(ILogger<WishesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(new {
                Id  = 1,
                Name = "Test",
                UserId = 1
            });
        }
    }
}
